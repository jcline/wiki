<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

## #nouveau on irc.freenode.net

All are welcome to join our ongoing development conversation in the [[IRC channel|irc://irc.freenode.net/nouveau]]. 

<a name="log2"></a> 
### IRC logs

* Current: [[http://people.freedesktop.org/~cbrill/dri-log/index.php?channel=nouveau|http://people.freedesktop.org/~cbrill/dri-log/index.php?channel=nouveau]] Christoph Brill's logs, from October 1st, 2009. 
* Due to catastrophic hardware failure at freedesktop.org, the logs from 17 October 2006 to October 2009 were lost 
* [[10 June 2006 to 15 October 2006|http://lumag.spb.ru/nouveau/irclogs/]] 

## The irregular Nouveau Development Companion (TiNDC)

Or have a look here, for a summary of the IRC logs for the time until: 

<a name="T2011"></a>  
### 2011

* [[February 27th|Nouveau Companion 44]] 
<a name="T2009"></a>  
### 2009

* [[March 20th|Nouveau Companion 43]] 
* [[January 21st|Nouveau Companion 42]] 
<a name="T2008"></a>  
### 2008

* [[December 21st|Nouveau Companion 41]] 
* [[October 29th|Nouveau_Companion_40]] 
* [[May 17th|Nouveau_Companion_39]] 
* [[April 5th|Nouveau_Companion_38]] 
* [[March 21st|Nouveau_Companion_37]] 
* [[March 7th|Nouveau_Companion_36]] 
* [[February 13th|Nouveau_Companion_35]] 
* [[January 30th|Nouveau_Companion_34]] 
* [[January 12th|Nouveau_Companion_33]] 
<a name="T2007"></a>  
### 2007

* [[December 23rd|Nouveau_Companion_32]] 
* [[November 23rd|Nouveau_Companion_31]] 
* [[November 11th|Nouveau_Companion_30]] 
* [[October 28th|Nouveau_Companion_29]] 
* [[October 14th|Nouveau_Companion_28]] 
* [[September 28th|Nouveau_Companion_27]] 
* [[August 25th|Nouveau_Companion_26]] 
* [[August 8th|Nouveau_Companion_25]] 
* [[July 21st|Nouveau_Companion_24]] 
* [[July 7th|Nouveau_Companion_23]] 
* [[June 28th|Nouveau_Companion_22]] 
* [[June 10th|Nouveau_Companion_21]] 
* [[May 26th|Nouveau_Companion_20]] 
* [[May 12th|Nouveau_Companion_19]] 
* [[April 25th|Nouveau_Companion_18]] 
* [[April 8th|Nouveau_Companion_17]] 
* [[March 17th|Nouveau_Companion_16]] 
* [[March 3rd|Nouveau_Companion_15]] 
* [[February 16th|Nouveau_Companion_14]] 
* [[February 1st|Nouveau_Companion_13]] 
* [[January 23rd|NouveauCompanion_12]] 
* [[January 9th|NouveauCompanion_11]] 
<a name="T2006"></a> 
### 2006

* [[December 24th|NouveauCompanion_10]] 
* [[December 9th|NouveauCompanion_9]] 
* [[November 25th|Nouveau_Companion_8]] 
* [[November 17th|NouveauCompanion_7]] 
* [[October 27th|NouveauCompanion_6]] 
* [[October 10th|NouveauCompanion_5]] 
* [[September 17th|NouveauCompanion_4]] 
* [[September 10th|NouveauCompanion_3a]] 
* [[September 1st|NouveauCompanion_2]] 
* [[August 26|NouveauCompanion_1]] 