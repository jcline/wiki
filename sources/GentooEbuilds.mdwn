<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

Please first read the official Gentoo wiki page on [[Nouveau|https://wiki.gentoo.org/wiki/Nouveau]].

#### Stable ebuilds:

Nouveau is now available from the stable branch of the portage tree. Enable DRM_NOUVEAU in your kernel and set VIDEO_CARDS="nouveau" in /etc/make.conf. For detailed instructions, see [[http://www.gentoo.org/doc/en/xorg-config.xml|http://www.gentoo.org/doc/en/xorg-config.xml]] 

#### Development ebuilds:

Previously, the nouveau ebuilds where in the **nouveau** overlay, which is now ******unmaintained******. Use the **x11** overlay instead, which is also available via layman 

Recommended ebuilds: 

* **x11-base/nouveau-drm** (from git) 
* **x11-drivers/xf86-video-nouveau** (from git) 
* **x11-libs/libdrm** (from git) 
The `x11` overlay also has a mesa git ebuild, which contains the gallium based nouveau 3d driver.
