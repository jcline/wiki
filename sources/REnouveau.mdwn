<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

**REnouveau has been superseeded by [[Valgrind-mmt|Valgrind-mmt]]. This page is here for historical reason.**

## Info:

REnouveau stands for Reverse Engineering for nouveau. KoalaBR maintains some (incomplete) [[documentation|http://www.ping.de/sites/koala/]]. If interest is high enough, it will be folded into CVS. 

REnouveau requires the **nVidia proprietary driver** installed and working.

It is an application that runs small opengl tests and watches the changes in the video card registers. Usage instructions are on the README file. It takes some time to get into, but once you know it, it is simple to read. 

This is used to do clean room reverse engineering (this is not in violation with nvidia driver license). We do **not** disassemble binaries. 

**NOTE** : It is unlikely that REnouveau dumps will turn up new information at the moment, but Fermi (not Enrico) will probably change that soon. 

If you are not sure about installing and running renouveau, please try KoalaBR's script, which does downloading, compiling, running and zip'ping automatically. You can find it here:  [[http://www.ping.de/sites/koala/script/createdump.sh|http://www.ping.de/sites/koala/script/createdump.sh]]. Just give execution rights and start it via `./createdump.sh`. Then follow the on screen instructions. Please note: Script comes without any warranty, use it on your own risk. 


### Wanting to do some reverse engineering ?

A shell script parse-dump.sh has been added to the CVS repository, that does all the dirty work of parsing your generated dumps. If everything went the right way, you can dig in the parsed dumps and try to guess what some random command send to your GPU do. 

Example of an unknown command (we have no idea what command is at offset 0x1538): 
[[!format txt """
00043538  size 1, subchannel 1 (0xbeef5097),offset 0x1538,increment
00000001    NV54TCL[0x1538/4]
"""]]
Example of a known command (as you can guess, STENCIL_BACK_MASK is at offset 0x1398): 
[[!format txt """
00043398  size 1, subchannel 1 (0xbeef5097),offset 0x1398,increment
000000ff    NV54TCL.STENCIL_BACK_MASK
"""]]

## Download:

CVS is available online: [[http://nouveau.cvs.sourceforge.net/viewvc/nouveau/renouveau/|http://nouveau.cvs.sourceforge.net/viewvc/nouveau/renouveau/]] 


### Anonymous CVS Access


[[!format txt """
cvs -z3 -d:pserver:anonymous@nouveau.cvs.sourceforge.net:/cvsroot/nouveau co -P renouveau
"""]]

### Gentoo

See [[Gentoo ebuilds|http://nouveau.freedesktop.org/wiki/GentooEbuilds]]. 


### Mandriva

Mandriva package is in the contrib media, just run 
[[!format txt """
urpmi renouveau
"""]]

### Debian and Ubuntu

Debian etch needs packages libsdl1.2-dev, nvidia drivers, libxvmc-dev, libxv-dev for sdl-config command and Xext, XvMCNVIDIA, XvMC, Xv library respectively. 

If you are running Ubuntu this command should get you the right packages: 


[[!format txt """
sudo apt-get install build-essential cvs mesa-common-dev libsdl1.2-dev libxvmc-dev nvidia-glx-dev dialog libxml2-dev
"""]]

### Fedora

For Fedora (tested on F9) with livna repository active 
[[!format txt """
yum install dialog SDL-devel libXvMC-devel libxml2-devel xorg-x11-drv-nvidia-devel
"""]]
then 
[[!format txt """
make XEXT_LDFLAGS="-L/usr/lib/nvidia -L/usr/lib -lXext -lXvMCNVIDIA -lXvMC -lXv"
"""]]
or, if you're on x86_64 
[[!format txt """
make XEXT_LDFLAGS="-L/usr/lib64/nvidia -L/usr/lib64 -lXext -lXvMCNVIDIA -lXvMC -lXv"
"""]]

## Requirements:

Functional nvidia drivers are needed, very old ones are not likely to work. 6xxx, 7xxx, 8xxx series are likely to work (based on a cvs commit log mentioning 6xxx support and assumption). 3xxx, 4xxx, 5xxx series may requires a 2.4 kernel, and renouveau must be linked against libGL at compile time. 

Your video card must have 256MB memory or less. As only 256MB of vram is accessible through the PCI address space for the CPU, we can not dump stuff if your card as more memory, as needed info may reside in non accessible vram space. 

**Note:** If you have driver version 9xxx and your card is PCI-E, renouveau may segfault or hang your machine. In this case, please try 8xxx drivers. 


## Compile Notes

REnouveau lacks a make install option, for the moment you'll have to manually copy the binary to the right place. main.c needs to be manually modified to do specific tests, please see documentation for more information. 

All tests and outputting to multiple files is now enabled by default. 


## Use and Submission Instructions

Please see [[REnouveauDumps|REnouveauDumps]]. 


## Problems


### renouveau does not compile: cannot find -lXvMCNVIDIA

This is usually due to the missing symbolic link `/usr/lib/libXvMCNVIDIA.so`. You can create it with something like 


[[!format txt """
ln -s /usr/lib/libXvMCNVIDIA.so.1.0.8776 /usr/lib/libXvMCNVIDIA.so
"""]]
as root. Substitute the name libXvMCNVIDIA.so.1.0.8776 with the version you have. 


### renouveau says "device detection failed"

Usually this is due to broken nvidia driver installation. `glxinfo` should report 


[[!format txt """
direct rendering: Yes
server glx vendor string: NVIDIA Corporation
...
client glx vendor string: NVIDIA Corporation
...
OpenGL vendor string: NVIDIA Corporation
"""]]
* If it does not, try to reinstall the proprietary driver from nvidia. 

### renouveau segfaults

Let me guess, you have a PCI-E card and nVidia driver version 9xxx. If you do, then it is a known "feature". The driver maps memory areas in and out and renouveau loses the track. You can try to downgrade to driver version 8xxx. If you do not have PCI-E and 9xxx, let us know. 

It would be useful to Renouveau developers if you could run renouveau under gdb. Instead of running renouveau, use the command `gdb ./renouveau` and you should get the gdb prompt. Command `run` and it should start renouveau and crash at some point, then you should be facing the gdb prompt again. Use the command `bt` and save the output. You can use e.g. [[http://rafb.net/paste/|http://rafb.net/paste/]] and post a link to #nouveau IRC channel. Finally, you can kill renouveau with command `kill` and exit gdb with `quit`. 


### renouveau: Creating probe window failed.

You are likely using a 16-bit color depth on your desktop. Please, try again with 24-bit colors. Some Renouveau tests requires the stencil buffer to be available, and it's only there in 24-bit color mode (24 bits for depth, 8 for stencil). 


### renouveau crashes in test X

Each test should be independent of each other. However there are still some tests that don't restore the OpenGL state like it was before running the test. For example, we have reports saying that nv_fragment_program2 test crashes sometimes if ran for the whole test list, but does not crash if it's the only test ran. Proper fix is to find which test ran before makes it crash, and try to rewrite it in a more proper way. 
