<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>


DDC (Display Data Channel) is a way for your graphic adapter to get in touch with your display device. It is also a set of [[VESA|http://www.vesa.org]] (Video Electronics Standards Association) standards. 


### real world

Physically, it is some wires in the cable which connects your graphic adapter to your display. And [[VESA|http://www.vesa.org]] defines a set of electrical protocols on those wires in order to achieve display related data exchange. Basically, you have 3 main _types_ of DDC: DDC 1, DDC 2B and E-DDC (The additional E stands for Enhanced). The first two are unidirectional, the last one is bidirectional. DDC 1 uses the vertical synchronization wires of your display cable. DDC 2B and E-DDC use independent wires of your display cable and are build upon a well known NXP (philips collective) standard: [[I2C|http://www.nxp.com/news/backgrounders/bg_esc9727/index.html]]. More on [[VGA connector DDC|http://www.hardwarebook.info/VGA_(VESA_DDC)]] and [[some electrical additional information|http://www.epanorama.net/documents/pc/vga_bd15.html]]. 


### what type of data

DDC can do a lot. But we are interested in one specific type of data: EDID (Extended Display Identification Data) and the more featured E-EDID (Enhanced Extended Display Identification Data). EDID is 128 bytes and E-EDID seems to have a basic size of 256 bytes but could reach 32kB. 

There is an available document for download, _the access bus_, published as a [[VESA free standard|http://www.vesa.org/Standards/free.htm]] ([[download access|http://www.vesa.org/thankspublic.htm]]) which gives a detailed description in its 7th section of what you can achieve with DDC. DDC 2AB seems to have been replaced with DDC/CI (Command Interface) and MCCS (Monitor Control Command Set).  


### how to reach the EDID and the E-EDID

DDC 2B and E-DDC are I2C buses. From the DDC 2B point of view, the EDID can be reach through the 0x50 (0xA0/0xA1 raw address bytes) address and has a size of 128 bytes. E-DDC can be reached through the 0x30 (0x60/0x61 raw address byte) address. 


### g94

It seems there are several I2C ports. There is a table in the VBIOS which maps the outputs to a I2C port that will be used as a . Basically, that means that the wires or a specific I2C port were wired by the board manufactured on a specific output connector(VGA/DVI connector). The nv driver references some registers in order to access the CLK and DATA bits of a I2C port. The base read/write register has offset 0x0000e138 an is 32 bits little endian. Bits: 

* 02 is the set flag. Use it to change the state of the I2C lines. 
* 01 is the data I2C line 
* 00 is the clock I2C line 
The other I2C DDC port register addresses are computed on a 0x18 base offset. Namely the second I2C DDC port register has an address of 0x0000e150. It seems that there is 4 I2C DDC ports based on mmio traces and blob log. 

It seems there are 2 additional I2C ports, still based on mmio trace and blob kernel log at 0x0000e254 and 0x000e274. Their use is probably for monitoring/fan management chips. If you run the strings command on the blob you will see that they are some references on such chips. There is more communication with 0x0000e274 which seems to be in sync with the fan lower speed switch. 

The timings for the I2C bus used as a DDC stolen from the frame buffer nvidia kernel module: 

* 2.2 ms for the timeout (VESA mandate 2 ms) 
* 40 µs of udelay 
On a POSTed board, I was unable to disable the I2C DDC port (which gets the EDID and/or the E-EDID) by disabling the Master Control register 0x00000200 bits set at by the NV driver (the nvidia open source driver). 

On a POSTed and non-POSTed board and a display with its DDC hardware on, I was able to get the EDID with a minimal linux module. 

Then, the I2C "engine" seems to be available all the time and independent of all the other "engines". 

**IMPORTANT NOTE:** Be careful, your display can go into a power state where the DDC is not available anymore. In theory, there is a +5v power wire paired with the vertical ground wire on VGA and DVI connectors which powers the display DDC hardware in those specific power states (but it's not the case with my CRT monitor). I used a voltmeter to check for +5V between VGA connector pin 10 and 9: All the time present, POSTed or non-POSTed, after the NV or blob driver X shutdown. The failure of my CRT monitor to handle the DDC +5v support would mean you cannot trust displays to make the EDID available when expected. 

TODO:patch Linux I2C in order to catch blob dialog with I2C devices. 
