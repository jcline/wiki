[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[TiNDC 2008|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_37]]/[[ES|Nouveau_Companion_37-es]]/[[FR|Nouveau_Companion_37-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 21 mars 2008


### Introduction

Bonjour, vous êtes sur le point de lire l'édition 37 du TiNDC. Si vous espérez que cette édition soit aussi longue que la dernière, préparez-vous à être quelque peu déçu. Nous revenons à la longueur normale cette fois-ci. (ndt : ouf!) 

Globalement, il n'y a pas de progrès évident à annoncer, puisque les gens essayent de jouer un peu avec Gallium ou travaillent à faire marcher Gallium sur leurs cartes. Si vous êtes courageux, vous pouvez l'essayer d'après le blog de Hanno Boeck ([[http://www.hboeck.de/archives/599-A-try-on-current-nouveau.html|http://www.hboeck.de/archives/599-A-try-on-current-nouveau.html]]) ou jetez un oeuil à notre page sur le Wiki : [[http://nouveau.freedesktop.org/wiki/GalliumHowto|http://nouveau.freedesktop.org/wiki/GalliumHowto]] 

Notez bien que nous ne pouvons et ne donnerons aucun support pour cela actuellement. Si ça marche, vous avez de la chance, sinon ne venez pas vous plaindre s'il-vous plaît. Il y a des bugs connus à la fois dans Nouveau et dans Gallium et nous y travaillons. Mais nous venons juste de commencer... 

La question a été levée de savoir si Nouveau devrait gérer des choses comme la vitesse des ventilateurs. La réponse est non, utilisez l'outil NVClock de Thunderbird pour ce genre de choses. 

Le dernier sujet pour cette introduction : nous avons reçu une 8400, offerte par kho. Cette carte ne quittera pas les petites mains avides de Marcheu qui va rejoindre les efforts de Darktama pour faire marcher la 3D sur NV50. Merci beaucoup kho pour ce généreux don! 

Le GSoC 2008 a vu Xorg accepté en tant qu'organisation mentor. D'après des résultats des deux dernières années, nous ne sommes pas très optimistes de pouvoir avoir un slot par Xorg. Si ça marche, nous aimerions travailler sur NV5x, donx si vous avez une 8x00 de côté que vous pouviez donner, n'hésitez pas. Et non, nous n'espérons pas que vous donniez une 8800 GTS ou une 9x00. Une autre 8400 ou 8500 devrait suffire! 

Et ça a été demandé quelques fois sur IRC : nous considérons les dernières G92 comme faisant partie de la famille NV50 et nous les supporterons dans le futur. Notez qu'il y a pour l'instant des inconvénients (plus d'informations là-dessus dans le prochain chapitre). 


### Statut actuel

Comme nous l'avions souligné dans la dernière édition, le _mode setting_ noyau va rentrer dans la branche principale bientôt. Fedora 9 sera la première à recevoir la tuyauterie adéquate  ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-03-06.htm#1251|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-03-06.htm#1251]]) 

Stillunknown et Malc0 ont continué à nettoyer et à corriger Randr1.2. Les _code path_ pour les NV50 ont également été nettoyés puisqu'il y avait pas mal de déchets qui s'étaient accumulés. La nouvelle édition est maintenant plus en phase avec le code des autres cartes. Cependant, aucune nouvelle fonctionnalité n'a été ajoutée, seuls les problèmes évident ont été résolus (notez que ni Malc0 ni Stillunknown n'ont de NV5x à leur disposition). Donc, le code marche aussi bien (ou aussi mal) qu'avant. 

Il reste cependant encore un peu d'espoir : Xorg-NV s'est vu ajouter du code pour parser les tables BIOS des cartes à base de NV50. Peut-être que malc0 sera capable de l'adapter à notre code. Il reste encore à voir ce qui pourra en sortir. 

Au-delà de ça, le calendrier pour Randr1.2 a été globalement confirmé, si aucun empêcheur de tourner en rond ne montre sa sale tête. En tout cas, Nouveau ne crashe pas en quittant X. Il essaye juste de restaurer le mode texte et échoue lamentablement, laissant une console texte inutilisable. 

Airlied a testé ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-03-12.htm#0053|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-03-12.htm#0053]]) Gallium sur PPC et a eu quelques problèmes avec le depth buffer. Une étude plus poussée par Darktama et Marcheu a dirigé les soupçons sur un possible problème dans Mesa PPC. Il semble y avoir un problème avec le depth buffer. Regardez l'engrenage vert pour avoir un aperçu du problème : 

[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_37?action=AttachFile&do=get&target=tindc25_gears_ppc.png] 

Darktama a un peu travaillé sur NV30, aidant pmdata à faire marcher l'initialisation de la carte. "J'ai juste branché la NV30GL pour le fun, et corrigé les problèmes principaux, le reste est entre les mains de pmdata maintenant :)". Benkai travaille toujours à résumer les options, problèmes et solutions prossibles empêchant de faire marcher le _suspend_ avec Nouveau. Après cela, il a commencé à tester la fonctionnalité de _suspend_ / _resume_ sur NV04 en modifiant le code du _suspend_ dans _EnterVT_ (le passage à la console texte) et pour le _resume_ dans _LeaveVT_. Donc en entrant dans une console texte, l'état était sauvé et restauré en revenant à X. 

Pour cette fonctionnalité, un _ioctl()_ a été rajouté au DRM et PGRAPH s'est avéré être trivial sur NV04 : Nouveau alloue simplement un canal et appelle engine->graph.save_context() avant l'initialisation de PGRAPH. Pour le _resume_, un appel à engine->graph.restore_context() fût suffisant. Après ça, Nouveau marchait après un cycle _LeaveVT_/_EnterVT_ (avec initialisation PMC/PTIMER/PFB/GPRAPH). 

Après ce succès, benkai a ajouté le _save_/_restore_ de tous les registres PFIFO à l' _ioctl()_. Nouveau a maintenant une interruption de changement de contexte PGRATH après que X ai recommencé à envoyer des commandes sur la FIFO. Malheureusement, la carte émet des erreurs étranges après cela. 

Comme déjà noté dans une édition précédente, veuillez trouver l'état actuel ici : [[http://nouveau.freedesktop.org/wiki/Suspend_support|http://nouveau.freedesktop.org/wiki/Suspend_support]] . Après avoir vu sa consommation électrique, Ahuillet fût également intéressé. 

Et maintenant, quelques informations à propos du NV50. À l'origine nous espérions que le NV50 aurait un mode de compatibilité NV40, mais cet espoir a été brisé rapidement. Avec le _reverse engineering_ un peu derrière et essayant de trouver son chemin autour de Gallium, Darktama a décidé de travailler sur NV4x où la plupart des choses requises sont déjà connues. 

Se concentrer sur NV4x a aidé Nouveau à faire les impressionnantes demos au LCA et au FOSDEM mais a laissé le NV50 dans le froid. Lorsque Darktama a changé sa NV43 pour une NV30 afin d'aider pmdata dans ses problèmes d'init, il a malheureusement cassé sa NV40. Donc face à la possibilité de travailler soit sur NV30 soit sur NV50, il a choisi le NV50 et s'est attaqué au moteur 3D. Après un peu de dur labeur il a découvert que le moteur 3D refuse de s'afficher sur des surfaces linéaires et que la seule manière de les rendre _carrelées_ est d'utiliser quelques flags dans les tables du GPU. 

La première chose qu'il a fait marcher fût l'effacement de buffers. Vint ensuite le DRM. Un changement qui empêche les canaux du GPU d'accéder directement à la VRAM. Ils vont maintenant à travers le système de mémoire virtuelle de la carte. C'est un pas en avant nécessaire vers le support des surfaces _carrelées_ qui sont requises pour la 3D. 

La chose suivant est d'essayer d'afficher des triangles. Cependant, cela demande quelques changements compliqués dans le DRM Nouveau. Il est prévu que les débuts du support Gallium viendra avant la composition EXA et nous aurons besoin de forcer toutes les images à être _carrelées_, et devoir gérer des cas de _fallback_ logiciel serait périlleux. Gallium est plus simple puisqu'il y aura besoin de beaucoup moins de _fallbacks_ logiciels. Le rendu est toujours effectué dans un buffer privé (même en effectuant un rendu sur le _"front buffer"_) et _blitté_ sur le vrai _front buffer_, on espère que le moteur 2D du NV50 est capable d'effectuer un _blit_ depuis une surface _carrelée_ sur une surface non _carrelée_. Ceci n'est cependant pas encore connu. 

Enfin, un petit assortiment de sujets: 

* Marcheu a fini son prototype de driver Gallium NV10 et l'a envoyé sur le dépôt. C'est maintenant à quelqu'un d'autre de venir l'y récupérer. 
* pq a posté son patch noyau MMioTrace pour vérification pour include au noyau (pour l'instant 2.6.25 rc). Aidez-le en testant MMioTrace chez vous si vous le pouvez. 
* Quelques autres causes de bloquages sur PPC ont été trouvées et réparées. 
* Depuis quelques temps, pq avait quelques problèmes avec les transferts AGP (principalement [[ImageFromCpu|ImageFromCpu]]). Ils ont été identifiés et à priori réparés. 
* Après que les problèmes d'init sur NV30 aient été finalement résolus, pmdata a commencé à travailler sur Gallium3D pour NV30 et a été quelque peu effrayé par les trucs des shaders. Il considère la possibilité de travailler sur NV10. 
* p0g a également commencé à travailler sur Gallium pour Nouveau. 

### Aide requise

Comme d'habitude, regarder sur notre page [[TestersWanted|TestersWanted]]. 

pq a besoin de votre aide pour tester le dernier MMioTrace. Ne vous proposez que si vous utilisez un noyau 2.6.25 rc récent. Les testeurs SMP sont également les bienvenus! 

Stillunknown cherche des testeurs NV5x pour clarifier que son ménage dans le code n'a rien cassé d'autre. Et si vous arriviez avec les MMioTraces du blob propriétaire du passage vers / depuis la console texte, ça serait aussi utile. Demandez conseil à pq, stillunknown ou malc0. 

Je crois que je ne l'ai pas mentionné dans les dernières éditions, mais nous accepterions volontiers des développeurs supplémentaires. Proposez-vous si l'état d'une certaine carte ou fonctionnalité vous déçoit :) 

Dernier point, mais pas le moindre : testez Randr1.2 avec Nouveau. Nous allons bientôt en faire le défaut si aucun problème n'apparaît. Et rappelez-vous : si vous ne testez pas, de mignons chatons vont mourrir! 

[[<<Édition précédente|Nouveau_Companion_36-fr]]  [[Édition suivante >>|Nouveau_Companion_38-fr]] 
