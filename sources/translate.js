function loadJs() {
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.charset = "UTF-8";
  js.src = "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit";
  var head = document.getElementsByTagName("head")[0];
  if (!head)
    head = document.body.parentNode.appendChild(document.createElement("head"));
  head.appendChild(js);
}

function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}

loadJs();
